#import ciphers.ciphers as ciphers
import modules.main_window as gui

__author__ = "Simone Romano"

'''
def cipherDebug():
    key = "password"
    message = "Hello Bob, how are you?"

    cipher = ciphers.AESCipher(key)
    print(cipher.getKey())

    encrypted = cipher.encrypt(message)
    print(encrypted)

    decrypted = cipher.decrypt(encrypted)
    print(decrypted)
'''

def main():
    gui.guiInit()

if __name__ == '__main__':
    main()
