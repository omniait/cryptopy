import sys
from cx_Freeze import setup, Executable

base = None
if sys.platform == "win32":
    base = "Win32GUI"

options = {
    'build_exe': {
        'includes': [
            'atexit',
            'ciphers',
            'main_window'
        ],
        'path': sys.path + ['modules']
    }
}

executables = [
    Executable('main.py', 
    icon='icon.ico',
    base=base)
]

setup(name='test1',
      version='0.1',
      description='',
      options=options,
      executables=executables
)