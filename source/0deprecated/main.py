from Crypto import Random
import Crypto as pyc
import base64
import string
import hashlib
import secrets


def main():
    key = "password"
    print(key)

    key = key.encode()

    hkey = hashlib.blake2b(key=key, digest_size=32)
    hkey.update(key)

    print(hkey.hexdigest())
    print(hkey.digest())

    iv = Random.new().read(pyc.Cipher.AES.block_size)
    cipher = pyc.Cipher.AES.new(hkey.digest(), pyc.Cipher.AES.MODE_CFB, iv)

    message = "ciao"

    print(message)

    message = str.encode(message)

    encrypted = cipher.encrypt(message)
    print(encrypted)

    decrypted = cipher.decrypt(encrypted)
    print(decrypted)

    decrypted.decode('utf-8')


if __name__ == '__main__':
    main()
