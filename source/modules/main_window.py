from PyQt5.QtWidgets import *
from PyQt5.QtCore import *
from PyQt5.QtGui import *
import modules.ciphers as ciphers

class Messenger(QVBoxLayout):
    def __init__(self):
        super(Messenger, self).__init__()

        cipher = None

        SubLayout1 = QVBoxLayout()
        SubLayout2 = QGridLayout()

        LabelKey = QLabel()
        LabelKey.setText("Shared Key")

        LineEditKey = QLineEdit()
        LineEditKey.setMaxLength(64)
        LineEditKey.setText("Secret Password")

        LabelHKey = QLabel()
        LabelHKey.setText("Hashed Key")

        LabelHKeyOut = QLabel()

        ButtonSubmit = QPushButton()
        ButtonSubmit.setText("Start Connection")
        ButtonSubmit.clicked.connect(
            lambda x: self.startConnection(LineEditKey, LabelHKeyOut))

        SubLayout1.addWidget(LabelKey)
        SubLayout1.addWidget(LineEditKey)

        SubLayout1.addWidget(LabelHKey)
        SubLayout1.addWidget(LabelHKeyOut)

        SubLayout1.addWidget(ButtonSubmit)

        # SubLayout2

        LabelAName = QLabel()
        LabelAName.setText("A Name: ")

        LabelBName = QLabel()
        LabelBName.setText("B Name: ")

        LineEditAname = QLineEdit()
        LineEditAname.setMaxLength(32)
        LineEditAname.setText("Alice")

        LineEditBname = QLineEdit()
        LineEditBname.setMaxLength(32)
        LineEditBname.setText("Bob")

        LabelHeader = QLabel()
        LabelHeader.setText("Messages")
        LabelHeader.setMaximumHeight(24)
        LabelHeader.setAlignment(Qt.AlignCenter)

        TextEditA = QTextEdit()
        TextEditA.setWordWrapMode(4)
        TextEditA.setReadOnly(True)

        TextEditB = QTextEdit()
        TextEditB.setWordWrapMode(4)
        TextEditB.setReadOnly(True)

        LabelWrite = QLabel()
        LabelWrite.setText("Write")
        LabelWrite.setMaximumHeight(24)
        LabelWrite.setAlignment(Qt.AlignCenter)

        LineEditAWrite = QLineEdit()
        LineEditAWrite.setMaxLength(64)

        LineEditBWrite = QLineEdit()
        LineEditBWrite.setMaxLength(64)

        PushButtonA = QPushButton()
        PushButtonA.setText("Send Message")
        PushButtonA.clicked.connect(
            lambda x: self.sendA2B(LineEditAname, LineEditAWrite, TextEditA, TextEditB))

        PushButtonB = QPushButton()
        PushButtonB.setText("Send Message")
        PushButtonB.clicked.connect(
            lambda x: self.sendB2A(LineEditBname, LineEditBWrite, TextEditB, TextEditA))

        SubLayout2.addWidget(LabelAName, 0, 0)
        SubLayout2.addWidget(LabelBName, 0, 1)

        SubLayout2.addWidget(LineEditAname, 1, 0)
        SubLayout2.addWidget(LineEditBname, 1, 1)

        #SubLayout2.setRowStretch(2, 2)
        SubLayout2.addWidget(LabelHeader, 2, 0, 1, 2)

        SubLayout2.addWidget(TextEditA, 3, 0)
        SubLayout2.addWidget(TextEditB, 3, 1)

        #SubLayout2.setRowStretch(4, 2)
        SubLayout2.addWidget(LabelWrite, 4, 0, 1, 2)

        SubLayout2.addWidget(LineEditAWrite, 5, 0)
        SubLayout2.addWidget(LineEditBWrite, 5, 1)

        SubLayout2.addWidget(PushButtonA, 6, 0)
        SubLayout2.addWidget(PushButtonB, 6, 1)

        self.addLayout(SubLayout1)
        self.addLayout(SubLayout2)

    def startConnection(self, LineEditKey, LabelHKeyOut):
        print("Starting Connection")

        key = LineEditKey.text()
        if key != "":
            self.cipher = ciphers.AESCipher(key)
            LabelHKeyOut.setText(str(self.cipher.getKey()))
        else:
            LabelHKeyOut.setText("Empty Key, using previous cipher initialisation")

    def sendA2B(self, LineEditAname, LineEditAWrite, TextEditA, TextEditB):
        author = LineEditAname.text()
        msg = LineEditAWrite.text()
        msg = str(msg)
        msg = msg.encode()
        LineEditAWrite.setText("")
        crypted = self.cipher.encrypt(msg)
        del msg

        TextEditA.append(author + " : " + str(self.cipher.decrypt(crypted)))
        #TextEditB.append(author + " : " + str(crypted))

        TextEditB.append("Secret Message From : " +
                         author + "\n : " + str(crypted))
        TextEditB.append("Decrypted :\n " + 
                         str(self.cipher.decrypt(crypted)))

    def sendB2A(self, LineEditBname, LineEditBWrite, TextEditB, TextEditA):
        author = LineEditBname.text()
        msg = LineEditBWrite.text()
        msg = str(msg)
        msg = msg.encode()
        LineEditBWrite.setText("")
        crypted = self.cipher.encrypt(msg)
        del msg

        TextEditB.append(author + " : " + str(self.cipher.decrypt(crypted)))
        #TextEditB.append(author + " : " + str(crypted))
        
        TextEditA.append("Secret Message From " + 
                         author + "\n : " + str(crypted))
        TextEditA.append("Decrypted :\n " + 
                         str(self.cipher.decrypt(crypted)))

class MainWindow(QMainWindow):
    # Constructor
    def __init__(self):
        super(MainWindow, self).__init__()

        self.setWindowTitle("Chat Demonstration")
        self.setWindowIcon(QIcon('icon.ico'))

        MainFrame = Messenger()

        widget = QWidget()
        widget.setLayout(MainFrame)
        widget.setMinimumWidth(750)

        self.setCentralWidget(widget)    

def guiInit():
    
    app = QApplication([])

    window = MainWindow()
    window.show()

    app.exec_()
