# Cryptopy

## Italiano

Trattasi di un programma sviluppato in Python che usa il cifrario AES al fine di scambiare informazioni in modo sicuro tra due persone che hanno già provveduto a scambiarsi una password in quanto l'AES è un Cifrario Simmetrico. 

Il programma **simula** un ambiente virtuale (chat) per scambiarsi messaggi in modo sicuro.

Si può pensare a delle similitudini con alcuni servizi di posta con crittografia End-To-End come Protonmail o Tutanota.

La struttura della cartella del sorgente (*source*) è la seguente:

```
.
├── 0deprecated
│   └── main.py
├── icon.ico
├── main.py
├── modules
│   ├── ciphers.py
│   ├── __init__.py
│   ├── main_window.py
│   └── __pycache__
│       ├── ciphers.cpython-36.pyc
│       ├── __init__.cpython-36.pyc
│       └── main_window.cpython-36.pyc
├── __pycache__
│   └── main.cpython-36.pyc
└── setup.py
```

``main.py`` è il file che chiama il modulo ``modules.main_window``.

``main_window.py`` è il file che gestisce l'interfaccia grafica dell'utente e contiene come oggetto una classe specificata in ``modules.ciphers`` che è costruito attorno l'AES ed espone dei metodi per cifrare, decifrare e ottenere la chiave.

Al fine di esser sicuri che la password abbia una lunghezza accettata dall'AES si usa la funzione hash SHA3 256 bit, applicare questa funzione consente anche password di lunghezza infinita (limitata però nella UI) come specificato nelle specifiche della funzione stessa.

È presente nella cartella parente del codice sorgente la specifica per linux per l'ambiente conda al fine di poter usare e creare un ambiente virtuale conda python facilmente, il nome del file è ``condaenv.yml``.

## English

It's a simple application developed in Python that uses the AES Cipher to exchange securely messages between two people that know a shared secret password, this is a limitation of the AES algorithm.

The application is only a simulation of a virtual environment (like a chat) used to securely exchange messages.

The process is similar to an email service that uses End-To-End Encryption like Protonmail or Tutanota.

The source folder structure is this:

```
.
├── 0deprecated
│   └── main.py
├── icon.ico
├── main.py
├── modules
│   ├── ciphers.py
│   ├── __init__.py
│   ├── main_window.py
│   └── __pycache__
│       ├── ciphers.cpython-36.pyc
│       ├── __init__.cpython-36.pyc
│       └── main_window.cpython-36.pyc
├── __pycache__
│   └── main.cpython-36.pyc
└── setup.py
```

``main.py`` is the file that calls the module ``modules.main_window``.

``main_window.py`` is the file that manages the Graphical User Interface(GUI) and uses the AES Cipher as an object imported from `modules.ciphers`, this class exposes some methods to encrypt, decrypt and to obtain the key.

To have a password compliant with AES specifications a Hash Function is used, this function is the SHA3 at 256 bits. SHA 3 permits also to have an infinite password but the password length is limited by the UI.

In the parent folder of the source code there is the Conda Environment Linux Specification so that you can create a Conda Virtual Environment easily, the file's name is ``condaenv.yml``.